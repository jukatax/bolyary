<?php 													// include the header.php(styles,scripts,meta)
$file = '../includes/header.php';
if (file_exists($file) && is_readable($file)) {
    include ($file);
}
?>
<meta name="description" content="Bolyary Restaurant Menu- Grill and Halal,Vegetarian and Desserts" />
<meta name="keywords" content="Grill,shkembe chorba,Kebap,halal,fish and chips,english breakfast,Pizza,Vegetarian dishes" />
<!--  <link rel="stylesheet" href="css/dg-picture-zoom.css" type="text/css" />
<script type="text/javascript" src="code/lib/external/mootools-1.2.4-core-yc.js"></script>
<script type="text/javascript" src="code/lib/external/mootools-more.js"></script>
<script type="text/javascript" src="code/lib/dg-picture-zoom.js"></script>
<script type="text/javascript" src="code/lib/dg-picture-zoom-autoload.js"></script> -->
<script	type='text/javascript' src="js/lightbox.js"></script>
<style>
/*search google styles*/
#textmenu table{width:100%;}
#textmenu table tr td{width:50%;text-transform:uppercase;}
input{color:#000;}
@media screen and (max-width: 768px) {
	#textmenu table tr td{width:75%;}
	}
</style>
</head>
<body itemscope itemtype="http://schema.org/WebPage">
<!-- GoogleTagManager =======================================================-->

<?php 													
	
$file='../includes/tagmanager.php'; 	
if(file_exists($file) && is_readable($file)){		
	include($file);
		}
?>
<!-- HEADER  =======================================================-->


<header itemscope itemtype="http://schema.org/Restaurant">
  <p class="head1"><a href="index.php">
<span itemprop="name">Bolyary Restaurant</span></a></p>
  <div class="hiding"></div>
  <p class="reservations"><!-- Book a table:<a href="bookings.php">Bookings form</a> <br />-->
    Call us: <span itemprop="telephone" onClick="ga('send', 'event', 'Click', 'Call', 'ClickCall');"><?php echo "{$tel}"; ?></span> </p>
</header>


<!-- END HEADER  ===================================================--> 
<!-- MENU  =========================================================-->
<nav>
  <?php  									//include the menu.php
    $file2 = '../includes/menu.php';
    if (file_exists($file2) && is_readable($file2)) {
        include ($file2);
    }
        ?>
    <!--<ul id="breadcrumbs" itemprop="breadcrumb">
        <li class="first"><a href="index.php">Home</a></li>
        <li> &gt;&gt; <em><?php echo "{$title}"; ?></em></li>
        </ul> --> 
  
</nav>

<!-- SITE SEARCH  ===================================================--> 
<!-- =========================================================-->
<!--
<script type="text/javascript">
// Google Internal Site Search script- By JavaScriptKit.com (http://www.javascriptkit.com)
// For this and over 400+ free scripts, visit JavaScript Kit- http://www.javascriptkit.com/
// This notice must stay intact for use
//Enter domain of site to search.
var domainroot="bolyary.co.uk"

function Gsitesearch(curobj){
curobj.q.value="site:"+domainroot+" "+curobj.qfront.value
}
</script>


<form action="http://www.google.com/search" method="get" onSubmit="Gsitesearch(this)">

<p style="text-align:left;font:italic .8em  Helvetica,serif;">
	Search Bolyary Restaurant for:<br />
	<input name="q" type="hidden" />
	<input name="qfront" type="text" style="width: 180px" /> 
	<input type="submit" value="Search" />
</p>

</form>

<div class="searchg">
<script>
  (function() {
    var cx = '000197158529147137415:njsp-wmpzly';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>
</div>
-->
<!-- END SITE SEARCH =========================================================-->
<!-- END MENU  =====================================================-->
<hr />
<h1 id='main'>Our Menu</h1>
<p class="offer">
	<strong>OFFERS:</strong>
	<br />
	 <!--<strong>Mixed Grill(for 4 people) only £25.Mixed Grill(for 2 people) only £15.<br />Valid for in-store purchases between 04-06/May </strong>
	<a id="fb" href="http://www.facebook.com/sharer.php?u=http://bolyary.co.uk/foodmenu.php"><img src="social/f.png" width="18" alt="facebook icon" /></a> -->
</p>
<!--<p><a href="#fullmenu" onClick="ga('send', 'event', 'Click', 'GoTo', 'ClickMenu');">Go to our FULL MENU CATALOG</a></p>-->
<div id="foodmenu">
  <h2>Main course</h2>
  <h3 class="menusubtitle"><img src="imgs/up.png" width="20" height="20" alt="up arrow" /> Grill <img src="imgs/up.png" width="20" height="20" alt="up arrow" /></h3>
  <div> 
    
    <!-- ========================= Connect to database ====================-->
    
    <?php
             /* */ $con = mysql_connect("mysql.0hosting.org", "u465187560_admin", "Thebest78");
           //$con = mysql_connect("localhost","root",""); //LOCALHOST settings
            if (!$con) {
                die('Could not connect: ' . mysql_error());
            }
            ?>
        <!-- ========================= Include the read file function ====================-->
        <?php
            $file6 = '../includes/readfiles.php';
            if (file_exists($file6) && is_readable($file6)) {
                include ($file6);
            }
            $dir = "imgs/menu/grill/";
            //<!-- ========================= Specify directory to read from ========================-->
            readfiles($dir);
            ?>
    <p>Fresh grilled pork, chicken and lamb meat for every taste. Traditional kyufte and kebabche dishes along with pleskavitsa and variety of steaks.Not all dishes are listed above. All grilles come with rice with vegetables, chips and salad as a side. </p>
  </div>
  <!--======================================= -->
  
  <h3 class="menusubtitle"><img src="imgs/down.png" width="20" height="20" alt="down arrow" /> Family Trays <img src="imgs/down.png" width="20" height="20" alt="down arrow" /></h3>
  <div>
    <?php
            $dir = "imgs/menu/familyhome/";
            readfiles($dir);
            ?>
    <p>Family Trays <em><strong>require a day notice</strong></em> and come to pick up next day or give us an address for free delivery. Many other dishes available. Call us for more info!<br />
	<em>NOTE: "Chirpansky style meatballs" includes six(6) of the pictured above portions.</em></p>
  </div>
  <!--======================================= -->
  
  <h3 class="menusubtitle"><img src="imgs/down.png" width="20" height="20" alt="down arrow" /> Specialties <img src="imgs/down.png" width="20" height="20" alt="down arrow" /></h3>
  <div>
    <?php
            $dir = "imgs/menu/specialties/";
            readfiles($dir);
            ?>
    <p>Traditional Bulgarian hot dishes like Kavarma, Satch and Gyuvech with pork or chicken meat with vegetables stewed, grilled or fried. Not all dishes are listed above!</p>
  </div>
  
  <!--======================================= -->
  
  <h3 class="menusubtitle"><img src="imgs/down.png" width="20" height="20" alt="down arrow" /> Seafood <img src="imgs/down.png" width="20" height="20" alt="down arrow" /></h3>
  <div>
    <?php
            $dir = "imgs/menu/seafood/";
            readfiles($dir);
            ?>
    <p>Huge variety of fresh seafood dishes including squid, prawns, trout and tuna steak.</p>
  </div>
  <!--======================================= -->
  
  <h3 class="menusubtitle"><img src="imgs/down.png" width="20" height="20" alt="down arrow" /> Vegetarian <img src="imgs/down.png" width="20" height="20" alt="down arrow" /></h3>
  <div>
    <?php
            $dir = "imgs/menu/vegetarian/";
            readfiles($dir);
            ?>
    <p>All kinds of crockets and vegetable dishes for all vegetarians out there. Main course vegetarian dishes are served with sides! Not all dishes are listed above </p>
  </div>
  
  <!--======================================= -->
  
  <h3 class="menusubtitle"><img src="imgs/down.png" width="20" height="20" alt="down arrow" /> From the oven <img src="imgs/down.png" width="20" height="20" alt="down arrow" /></h3>
  <div>
    <?php
            $dir = "imgs/menu/oven/";
            readfiles($dir);
            ?>
    <p>A variety of meat and mushroom dishes from the oven for maximum taste.</p>
  </div>
  
  <!--======================================= -->
  
  <h3 class="menusubtitle"><img src="imgs/down.png" width="20" height="20" alt="down arrow" /> Pizza <img src="imgs/down.png" width="20" height="20" alt="down arrow" /></h3>
  <div>
    <?php
            $dir = "imgs/menu/pizza/";
            readfiles($dir);
            ?>
    <p>Freshly made italian style pizzas. <strong>Note:</strong> All pizzas except vegetarian have yellow cheese topping! Not all pizzas are listed above! </p>
  </div>
  
  <!--======================================= -->
  
  <h3 class="menusubtitle"><img src="imgs/down.png" width="20" height="20" alt="down arrow" /> Desserts <img src="imgs/down.png" width="20" height="20" alt="down arrow" /></h3>
  <div>
    <?php
            $dir = "imgs/menu/desserts/";
            readfiles($dir);
            ?>
    <p>Traditional Bulgarian home-made Biscuit cake and Salami cake(biscuits,cocoa,nuts and Bulgarian delight),pancakes and ice cream! </p>
  </div>
  
  <hr />
  <!-- ============================================ FULL MENU ==================================================== 
    <h3 class="menusubtitle" id="fullmenu" onClick="ga('send', 'event', 'Click', 'GoTo', 'ClickMenu');"><img src="imgs/down.png" width="20" height="20" alt="down arrow" />FULL FOOD MENU<img src="imgs/down.png" width="20" height="20" alt="down arrow" /></h3>
  <div id="textmenu">
   <h4>Salads</h4>
   <table>
		<tr><td>Shopska</td> <td>£4,80</td></tr>
		<tr><td>Shepherds</td> <td>£4,80 </td></tr>
		<tr><td>Village style salad</td> <td>£5,80</td></tr>
		<tr><td>RAZIADKA</td> <td>£4,80</td></tr>
		<tr><td>Bolyarska</td> <td>£5,50</td></tr>
		<tr><td>Balkans’ Topenica</td> <td>£4,20</td></tr>
		<tr><td>Katak</td> <td>£4,20</td></tr>
		<tr><td>Staroselska</td> <td>£4,20</td> </tr>                                                                                                        
		<tr><td>Milk salad</td> <td>£4,20 </td></tr>                                                                       
		<tr><td>CABBAGE AND CARROTS SALAD</td> <td>£3,50</td></tr>
		<tr><td>Grilled vegetables</td> <td>£8,50</td></tr>                                                                                                        
		<tr><td>The wolf meat meze</td> <td>£13,80</td></tr>
   </table> 
   
   <h4>HOT STARTERS</h4>
   <table>
		<tr><td>Breaded yellow chees</td> <td>£4,80</td></tr>
		<tr><td>Breaded feta cheese</td> <td>£4,80</td></tr>
		<tr><td>Crunchy breaded yellow cheese</td> <td>£4,80</td></tr>
		<tr><td>Crunchy breaded feta cheese</td> <td>£4,80</td></tr>
		<tr><td>Pepper burek</td> <td>£4,80</td></tr>
		<tr><td>Village style chicken hearts</td> <td>£5,50</td></tr>
		<tr><td>Grilled chicken hearts</td> <td>£5,50</td></tr>
		<tr><td>Village style chicken liver</td> <td>£5,50</td></tr>
		<tr><td>Village style pork liver</td> <td>£5,50</td></tr>
		<tr><td>Beef belly in butter</td> <td>£4,80</td></tr>
		<tr><td>Chicken nuggets</td> <td>£4,80</td></tr>
		<tr><td>Beef tongue in butter</td> <td>£5,80</td></tr>
		<tr><td>Breaded beef tongue</td> <td>£5,80</td></tr>
		<tr><td>Beef tongue grilled</td> <td>£5,80</td></tr>
		<tr><td>Stuffed potato</td> <td>£5,80</td></tr>
		<tr><td>Stuffed mushrooms</td> <td>£5,80</td></tr>
		<tr><td>Vegetarian burger</td> <td>£5,80</td></tr>
		<tr><td>Mish mash</td> <td>£4,80</td></tr>
		<tr><td>OMELETTE</td> <td>£4,80</td></tr>
		<tr><td>Fried potatoes</td> <td>£2,50</td></tr>
		<tr><td>Fried potatoes with feta cheese</td> <td>£3,00</td></tr>
		<tr><td>Boiled vegetables</td> <td>£4,00</td></tr>
		<tr><td>Rice with vegetables</td> <td>£3,00</td></tr>
		<tr><td>KAIK BITES</td> <td>£26,80</td></tr>
   </table>
   
   <h4>GRILL</h4>
   <table>
		<tr><td>COLLAR PORK STEAK</td> <td>£9,50</td></tr>
		<tr><td>PORK FILLETS</td> <td>£9,50</td></tr>
		<tr><td>PORK CHOPS</td> <td>£10,80</td></tr>
		<tr><td>CHICKEN THIGH STEAK</td> <td>£9,80</td></tr>
		<tr><td>CHICKEN FILLET STEAK</td> <td>£9,80</td></tr>
		<tr><td>CHICKEN STRIPS</td> <td>£9,80</td></tr>
		<tr><td>CHICKEN KEBAB</td> <td>£9,80</td></tr>
		<tr><td>PORK KEBAB</td> <td>£8,50</td></tr>
		<tr><td>CHICKEN SHASHLIK</td> <td>£9,50</td></tr>
		<tr><td>CHICKEN SHASHLIK  for two</td> <td>£18,50</td></tr>
		<tr><td>PORK SHASHLIK</td> <td>£9,80</td></tr>
		<tr><td>PORK SHASHLIK for two</td> <td>£18,50</td></tr>
		<tr><td>PORK RIBS</td> <td>£9,80</td></tr>
		<tr><td>LAMB CHOPS</td> <td>£12,80</td></tr>
		<tr><td>BEEF SIRLOIN STEAK</td> <td>£15,80</td></tr>
		<tr><td>KUYFTETA</td> <td>£8,80</td></tr>
		<tr><td>KEBABCHETA</td> <td>£8,80</td></tr>
		<tr><td>BOLYARSKY GRILL</td> <td>£9,80</td></tr>
		<tr><td>TATARSKO KYUFTE</td> <td>£9,20</td></tr>
		<tr><td>SCHNITZEL  VIENNA</td> <td>£8,50</td></tr>
		<tr><td>ITALIAN STYLE SCHNITZEL</td> <td>£8,50</td></tr>
		<tr><td>PLJESKAVICA</td> <td>£8,50</td></tr>
		<tr><td>CHICKEN WINGS</td> <td>£7,90</td></tr>
		<tr><td>SAUSAGES</td> <td>£8,50</td></tr>	
		<tr><td>BOLYARSKY KAIK</td> <td>£44,99</td></tr>
		<tr><td>MIXED GRILL PLATTER FOR 2</td> <td>£20,80</td></tr>	
		<tr><td>MIXED GRILL PLATTER FOR 4</td> <td>£39 ,80</td></tr>	
   </table>
   
   <h4>SPECIALS</h4>
   <table>
		<tr><td>Pork collar steak with mushrooms</td> <td>£8,80</td></tr>
		<tr><td>Chicken tight steak with mushrooms</td> <td>£8,00</td></tr>
		<tr><td>Chicken wings</td> <td>£7,50</td></tr>
		<tr><td>Mushrooms with bacon and cheese</td> <td>£7,50</td></tr>
		<tr><td>CHICKEN SPINDLE</td> <td>£9,80</td></tr>
		<tr><td>PORK SPINDLE</td> <td>£9,80</td></tr> 
		<tr><td>CHICKEN SATCH</td> <td>£9,50 </td></tr>
		<tr><td>CHICKEN SATCH large ,for 4 people</td> <td>£28,80</td></tr>
		<tr><td>PORK SATCH</td> <td>£9,50</td></tr> 
		<tr><td>PORK SATCH large for 4 people</td> <td>£28,80</td></tr>
		<tr><td>MIXED MEAT SATCH</td> <td>£9,50 </td></tr>
		<tr><td>MIXED MEAT SATCH large for 4 people</td> <td>£28,80</td></tr>
		<tr><td>CHEF’S SPECIAL SATCH</td> <td>£9,80</td></tr>
		<tr><td>CHEF’S SPECIAL SATCH large for 4 people</td> <td>£28,80</td></tr>
		<tr><td>CHICKEN JULLIEN</td> <td>£8,60</td></tr>
		<tr><td>PORK KAVARMA</td> <td>£8,80</td></tr>
		<tr><td>CHICKEN KAVARMA</td> <td>£8,80</td></tr>
		<tr><td>STAROBULGARSKA KAVARMA pork or chicken</td> <td>£8,50</td></tr>
		<tr><td>ROASTED LAMB SHANK</td> <td>£12,80</td></tr>
		<tr><td>ROASTED LAMB LEG TO SHARE</td> <td>£22,90</td></tr>
		<tr><td>STUFFED CHICKEN STEAK</td> <td>£8,50</td></tr>
		<tr><td>STUFFED PORK STEAK</td> <td>£8,50</td></tr>
		<tr><td>ZAPEKANKA</td> <td>£7,50</td></tr>
   </table>
   
   <h4>VEGETARIAN</h4>
   <table>
	    <tr><td>COURGETTE ROLLS WITH FETA CHEESE</td> <td>£7,80</td></tr>
		<tr><td>LENTIL CROCKETS</td> <td>£7,50</td></tr>
		<tr><td>SPINACH CROCKETS</td> <td>£7,50</td></tr>
		<tr><td>MUSHROOM CROCKETS</td> <td>£7,50</td></tr>
		<tr><td>MUSHROOM PANCAKES</td> <td>£6,50</td></tr>
   </table>
   
   <h4>GUVECH</h4>
   <table>
	   <tr><td>SHOPSKI STYLE FETA CHEESE</td> <td>£7,50</td></tr>
	   <tr><td>SAUSAGE AND VEGETABLES</td> <td>£7,50</td></tr>
	   <tr><td>CHICKEN GUVECH</td> <td>£8,40</td></tr>
	   <tr><td>PORK GUVECH</td> <td>£8,40</td></tr>
	   <tr><td>NASHENSKO GUVECH</td> <td>£8,40</td></tr>
   </table>
   
   <h4>PIZZA</h4>
   <table>
	   <tr><td>MARGARITA</td> <td>£5,50/£7,00</td></tr>
	   <tr><td>VEGETARIAN</td> <td>£5,80/£7,30</td></tr>
	   <tr><td>FUNGI</td> <td>£5,80/£7,30</td></tr>
	   <tr><td>ROMA</td> <td>£6,30/£7,50</td></tr>
	   <tr><td>ADRIANO</td> <td>£6,30/£7,50</td></tr>
	   <tr><td>CAPRICHOSA</td> <td>£6,30/£7,50</td></tr>
	   <tr><td>PESTO</td> <td>£6,30/£7,50</td></tr>
	   <tr><td>FROUTI DI MARE</td> <td>£6,80/£7,80</td></tr>
	   <tr><td>QUATRO STAGIONI</td> <td>£6,30/£7,50</td></tr>
	   <tr><td>QUATRO FORMAGIO</td> <td>£6,80/£7,80</td></tr>
	   <tr><td>CALZONE</td> <td>£6,50/£7,80</td></tr>
	   <tr><td>PIPERONE</td> <td>£6,50/£7,80</td></tr>
	   <tr><td>BOLYARY</td> <td>£7,30/£8,50</td></tr>
	   <tr><td>PARLENITSA</td> <td>£1.00/£1.50 </td></tr>
   </table>
   
   <h4>DAILY SPECIALS</h4>
   <table>
		<tr><td>EVERY DAY SOUPS pork belly and chicken soup</td> <td>£3,50</td></tr>
		<tr><td>HOMEMADE SPECIAL</td> <td>£6,20</td></tr>
		<tr><td>SOUP OF THE DAY</td> <td>£3,50</td></tr>
		<tr><td>TARATORv£3,50</td></tr>
   </table>
   
   <h4>DESSERTS</h4>
   <table>
		<tr><td>ICE CREAM MELBA</td> <td>£4,50</td></tr>
		<tr><td>PANCAKES</td> <td>£4,00</td></tr>
		<tr><td>BISCUIT CAKE</td> <td>£3,50</td></tr>
		<tr><td>SALAMI CAKE</td> <td>£3,50</td></tr>
		<tr><td>FRUIT SALAD</td> <td>£5,80</td></tr>
		<tr><td>OREO BISCUIT CAKE</td> <td>£3.00</td></tr>
		<tr><td>CUSTORD CAKE</td> <td>£3.50</td></tr>
   </table>
  </div>
  -->
  <!--<p> Download our full menu - <a href="pdf/smallmenu.pdf" title="Download our menu in pdf format">menu.pdf</a>. File size 1.92MB. </p>
  <br />
  <p> You need Acrobat Reader to view our menu on a PC.If you don't have it already,you can download it from Abobe's site:<a href="http://get.adobe.com/uk/reader/" rel="nofollow">Acrobat Reader</a> </p>-->
  <hr />
  <p><em>Bolyary Restaurant&trade; has the right to change prices and menu at any time without notification!</em></p>
</div>
<!-- =======================section starts  =====================================================-->
<section> </section>
<!-- ============================END section=====================================================-->
<div class="right"> </div>
<script type="text/javascript">
        foodclicks();
    </script> 
<!-- Image preview function to call from lightbox.js - jQuery -->
<?php
    mysql_close($con);
    // <!-- ==== DB disconnect=====================================================-->
    ?>
<script>
        //<!-- Hide the food divs but the first one and animate toggle them upon click - jQuery -->
        $('#foodmenu > div').hide();
        $('#foodmenu > div:first').show();
		$('#foodmenu > div >.food').css({'display':'none'});
		$('#foodmenu > div:first >.food').css({'display':'inline'});
        $('#foodmenu h3').click(function() {
			
            $(this).next().animate({
                'height' : 'toggle'
            }, 500, 'swing');
			$(this).next('div:first').children('.food').css({'display':'inline'});
			//$('#foodmenu > div >.food').css({'display':'inline'});
            if ($(this).children("img").is("[src*='up']")) {
                $(this).children("img").attr("src", "imgs/down.png");
            } else {
                $(this).children("img").attr("src", "imgs/up.png");
            };
        });
    </script> 
<!--============================================================= FOOTER ==========================================================================-->
<?php  													//include the footer.php
    $file3 = '../includes/footer.php';
    if (file_exists($file3) && is_readable($file3)) {
        include ($file3);
    }
    ?>