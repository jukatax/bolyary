<?php 													// include the header.php(styles,scripts,meta)
	$file='includes/header.php'; 
	if(file_exists($file) && is_readable($file)){
		include($file);
	}
?>
		
		<meta name="description" content="Book a table at Bolyari Restaurant" />
		<meta name="keywords" content="Bulgarian restaurant,restaurant,bulgarian cousine,bulgarian food,bulgarian restaurant in north london,grill,shkembe chorba,kebap,kyofte,french fries,chips,english breakfast,pizza,barbeque" />
	</head>

	<body itemscope itemtype="http://schema.org/WebPage">
			<!-- HEADER  =======================================================-->
			<header>
				<p class="head1"><a href="index.php">Bolyary Restaurant</a></p>
				<p class="reservations">Book a table: <a href="bookings.php">Bookings form</a>
				<br />Call us: <?php echo "{$tel}"; ?> </p>
			</header>
			<!-- END HEADER  ===================================================-->			
			<!-- MENU  =========================================================-->
			
			<nav>
				<?php  									//include the menu.php
					$file2='includes/menu.php';
					if(file_exists($file2) && is_readable($file2)){
						include($file2);
					}
				?>
				
			<!--	<ul id="breadcrumbs" itemprop="breadcrumb">                         
  					<li class="first"><a href="index.php">Home</a></li>
  					<li> &gt;&gt; <em><?php echo "{$title}"; ?></em></li>
  				</ul> -->

			</nav>
			<!-- END MENU  =====================================================-->
			<h1>About us</h1>
			<div class="left">
				<em>We are located in Palmers Green:<br />396 Green lanes<br />N13 5PD</em>
				<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.uk/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=396+Green+Lanes+Palmers+Green,+London,+Greater+London+N13+5PD,+UK&amp;sll=52.8382,-2.327815&amp;sspn=11.16101,23.444824&amp;ie=UTF8&amp;hq=&amp;hnear=396+Green+Lanes,+London+N13+5PD,+United+Kingdom&amp;ll=51.621553,-0.105588&amp;spn=0.005595,0.011448&amp;t=m&amp;z=14&amp;output=embed"></iframe><br />
				<small><a href="https://maps.google.co.uk/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=396+Green+Lanes+Palmers+Green,+London,+Greater+London+N13+5PD,+UK&amp;sll=52.8382,-2.327815&amp;sspn=11.16101,23.444824&amp;ie=UTF8&amp;hq=&amp;hnear=396+Green+Lanes,+London+N13+5PD,+United+Kingdom&amp;ll=51.621553,-0.105588&amp;spn=0.005595,0.011448&amp;t=m&amp;z=14" style="color:#0000FF;text-align:left">View Larger Map</a></small>
			</div>
			
			
			
			<!-- form starts  =====================================================-->
			
			
			<section>
							
			</section>
			<!-- END form  =====================================================-->
			<div class="right">
				
				
			</div>
				
			
			<!-- FOOTER ========================================================-->
<?php  													//include the footer.php
	$file3='includes/footer.php';
	if(file_exists($file3) && is_readable($file3)){
		include($file3);
	}
?>
			
			