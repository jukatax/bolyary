<?php 													
	
$file='../includes/header.php'; 	
if(file_exists($file) && is_readable($file)){		
	include($file);
		}
?>

<meta name="description" content="Bolyary Bulgarian Restaurant - sitemap" />
<meta name="keywords" content="Bulgarian Restaurant in North London" />
<!--<script	type='text/javascript' src="js/lightbox.js"></script>-->
</head>
<body itemscope itemtype="http://schema.org/WebPage">
<!-- GoogleTagManager =======================================================-->

<?php 													
	
$file='../includes/tagmanager.php'; 	
if(file_exists($file) && is_readable($file)){		
	include($file);
		}
?>
<!-- HEADER  =======================================================-->
<header><span itemscope itemtype="http://schema.org/Restaurant">
  <p class="head1"><a href="index.php"><span itemprop="name">Bolyary Restaurant</span></a></p>
  <div class="hiding"></div>
  <p class="reservations"><!-- Book a table:<a href="bookings.php">Bookings form</a> <br />--> Call us: <span itemprop="telephone"><?php echo "{$tel}"; ?></span> </p>
  </span> </header>
<!-- END HEADER  ===================================================--> <!-- MENU  =========================================================-->
<nav>
  <?php  									
					
  $file2='../includes/menu.php';					
  if(file_exists($file2) && is_readable($file2)){						
  	include($file2);					
	}				
?>
  <!--<ul id="breadcrumbs" itemprop="breadcrumb">                           					<li class="first"><a href="index.php">Home</a></li>  					<li> &gt;&gt; <em><?php echo "{$title}"; ?></em></li>  				</ul> --> 
</nav>
<hr />
<!-- END MENU  =====================================================-->
<h1>Sitemap</h1>
<div class="left">
  <ul class="sitemap">
    <li><a href="index.php">Home</a></li>
    <li><a href="foodmenu.php">Our Menu</a></li>
    <li><a href="bookings.php">Bookings</a></li>
    <li><a href="location.php">Location</a></li>
    <li><a href="privacy.php">Privacy Policy</a></li>
  </ul>
</div>
<!-- FOOTER ========================================================-->
<?php  													
	
$file3='../includes/footer.php';	
if(file_exists($file3) && is_readable($file3)){		
include($file3);	
}
?>
