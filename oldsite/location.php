<?php 													// include the header.php(styles,scripts,meta)
	$file='../includes/header.php'; 
	if(file_exists($file) && is_readable($file)){
		include($file);
	}
?>
		
		<meta name="description" content="Find Bolyary Restaurant in Palmers Green,North London" />
		<meta name="keywords" content="Bulgarian Restaurant in London,North London Restaurant,Restaurant near me,Cheap Restaurant" />
	</head>

	<body itemscope itemtype="http://schema.org/Restaurant">
	<!-- GoogleTagManager =======================================================-->

<?php 													
	
$file='../includes/tagmanager.php'; 	
if(file_exists($file) && is_readable($file)){		
	include($file);
		}
?>
			<!-- HEADER  =======================================================-->

<header>
  <p class="head1"><a href="index.php">
<span itemprop="name">Bolyary Restaurant</span></a></p>
  <div class="hiding"></div>
  <p class="reservations"><!-- Book a table:<a href="bookings.php">Bookings form</a> <br />-->
    Call us: <span itemprop="telephone" onClick="ga('send', 'event', 'Click', 'Call', 'ClickCall');"><?php echo "{$tel}"; ?></span> </p>
</header>

			<!-- END HEADER  ===================================================-->			
			<!-- MENU  =========================================================-->
			
			<nav>
				<?php  									//include the menu.php
					$file2='../includes/menu.php';
					if(file_exists($file2) && is_readable($file2)){
						include($file2);
					}
				?>
				
			<!--	<ul id="breadcrumbs" itemprop="breadcrumb">                         
  					<li class="first"><a href="index.php">Home</a></li>
  					<li> &gt;&gt; <em><?php echo "{$title}"; ?></em></li>
  				</ul> -->

			</nav>
			<hr />
			<!-- END MENU  =====================================================-->
			<h1 id='main'>Our location</h1>
			
			
			<div class="left">
				<iframe width="425" title="Google map of Bolyary Restaurant" height="350" src="https://maps.google.co.uk/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=396+Green+Lanes+Palmers+Green,+London,+Greater+London+N13+5PD,+UK&amp;sll=52.8382,-2.327815&amp;sspn=11.16101,23.444824&amp;ie=UTF8&amp;hq=&amp;hnear=396+Green+Lanes,+London+N13+5PD,+United+Kingdom&amp;ll=51.621553,-0.105588&amp;spn=0.005595,0.011448&amp;t=m&amp;z=14&amp;output=embed">Where to find us.Google map.</iframe><br />
				<small><a href="https://maps.google.co.uk/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=396+Green+Lanes+Palmers+Green,+London,+Greater+London+N13+5PD,+UK&amp;sll=52.8382,-2.327815&amp;sspn=11.16101,23.444824&amp;ie=UTF8&amp;hq=&amp;hnear=396+Green+Lanes,+London+N13+5PD,+United+Kingdom&amp;ll=51.621553,-0.105588&amp;spn=0.005595,0.011448&amp;t=m&amp;z=14" style="text-align:left">View Larger Map</a></small>
			</div>
			<div class="right">
				<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><strong>We are located in Palmers Green: </strong><br />
				<span itemprop="addressLocality">North London</span><br /><span itemprop="streetAddress">396 Green lanes</span><br /><span itemprop="postalCode">N13 5PD</span></p>
				<h2>Directions</h2>
				<p><strong>Nearest bus stops: <br /></strong><em>Bourne Hill(Bus 329 from Enfield/Wood Green)</em><br />
								<em>Fox Lane(Bus W6 from Southgate/Edmonton)</em></p>
				<p><strong>Nearest train station: </strong><em>Palmers Green</em></p>
				<p><strong>Nearest tube stations: </strong><em>Southgate</em> and <em>Wood Green</em></p>				
				
			</div>

			<!-- form starts  =====================================================-->
			
			
			<section>
							
			</section>
			<!-- END form  =====================================================-->
					
			<!-- FOOTER ========================================================-->
<?php  													//include the footer.php
	$file3='../includes/footer.php';
	if(file_exists($file3) && is_readable($file3)){
		include($file3);
	}
?>
			
			