<?php 													
	
$file='../includes/header.php'; 	
if(file_exists($file) && is_readable($file)){		
	include($file);
		}
?>

<meta name="description" content="Bolyary Bulgarian Restaurant - Christmas Holidays menu 2013" />
<meta name="keywords" content="Christmas menu, New years menu" />
<!--<script	type='text/javascript' src="js/lightbox.js"></script>-->
<style>
h1, h2, h3, h4{
	font-family:Verdana, sans-serif, Tahoma;color:#555;
	}
.chrmenu,.chrmenu2{background:#E8AD7B;border-radius:5px;margin:1em .25em;padding:.5em;color:#333;}
.sitemap li{margin:.5em 0;text-transform:lowercase;}
.sitemap li strong,.sitemap li p strong{text-transform:uppercase;}
.red{color:#f00;}
</style>
</head>
<body itemscope itemtype="http://schema.org/WebPage">
<!-- GoogleTagManager =======================================================-->

<?php 													
	
$file='../includes/tagmanager.php'; 	
if(file_exists($file) && is_readable($file)){		
	include($file);
		}
?>
<!-- HEADER  =======================================================-->
<header><span itemscope itemtype="http://schema.org/Restaurant">
  <p class="head1"><a href="index.php"><span itemprop="name">Bolyary Restaurant</span></a></p>
  <div class="hiding"></div>
  <p class="reservations"><!-- Book a table:<a href="bookings.php">Bookings form</a> <br />--> Call us: <span itemprop="telephone"><?php echo "{$tel}"; ?></span> </p>
  </span> </header>
<!-- END HEADER  ===================================================--> <!-- MENU  =========================================================-->
<nav>
  <?php  									
					
  $file2='../includes/menu.php';					
  if(file_exists($file2) && is_readable($file2)){						
  	include($file2);					
	}				
?>
</nav>
<hr />
<!-- END MENU  =====================================================-->
<h1>Special Holiday Menu</h1>
<!--
<h2 class="koledno">CHRISTMAS MENU - 25 Dec 2013</h2>
<h2 class="second">КУВЕРТ- &pound;30 НА ЧОВЕК/ENTRANCE FEE &pound;30 PER PERSON</h2>
<div class="chrmenu">
  <ul class="sitemap">
    <li><strong>САЛАТА/SALAD</strong> – СЕЛСКА РЕДЕНА САЛАТА  ОТ ДОМАТ, КРАСТАВИЦА, ЧУШКА И КАТЪК / VILLAGE STYLE SALAD(TOMATOES, CUCUMBERS AND PEPER) AND KATAK</li>
    <li><strong>ТОПЛО ПРЕДЯСТИЕ/HOT STARTER</strong>- ЧУШКА БЮРЕК / PEPPER BUREK</li>
    <li><strong>ОСНОВНО ЯСТИЕ/MAIN COURSE</strong>- СВИНСКО ПЕЧЕНО С ГЪБЕН СОС , КАРТОФЕНО ПЮРЕ , ПЕЧЕНО ПИЛЕШКО СЪС ЗЕЛЕ НА ФУРНА , ЗАДУШЕНИ ЗЕЛЕНЧУЦИ / ROASTED PORK WITH MUSHROOM SOUCE, MASHED POTATOES,ROASTED CHICKEN WITH CABBAGE, STEWED VEGGIES.</li>
    <li><strong>ПЪРЛЕНКА/PARLENITSA</strong>- СЪС СИРЕНЕ И КAШКАВАЛ / WITH FETA CHEESE AND YELLOW CHEESE</li>
	<li><strong>ДЕСЕРТ/DESSERT</strong>-  ДОМАШЕН ТИКВЕНИК / HOME MADE SWEET PUMPKIN PIE</li>
    <li><strong>ЕДНО БЕЗАЛКОХОЛНО И ЕДНА МАЛКА БУТИЛКА МИНЕРАЛНА ВОДА/ONE FIZZY DRINK AND SMALL MINERAL WATER</strong></li>
</ul><br />
<p><strong>ЗА РЕЗЕРВАЦИИ  МОЛЯ ОБЪРНЕТЕ СЕ КЪМ ПEРСОНАЛА В РЕСТОРАНТА ИЛИ <a href="bookings.php">РЕЗЕРВИРАЙТЕ ОНЛАЙН</a> сега.
<span class="red">ЗА ДА ПОТВЪРДИТЕ СВОЕТО МЯСТО ЗА 25 ДЕКЕМВРИ ТРЯБВА ДА ЗАКУПИТЕ СВОЯ КУВЕРТ ИЛИ ДА ОСТАВИТЕ ДЕПОЗИТ ОТ <em>9 ДЕКЕМВРИ</em></span>.</strong></p>
<p><strong>FOR RESERVATIONS PLEASE CALL OR <a href="bookings.php">BOOK ONLINE</a> NOW. <span class="red">TO CONFIRM YOUR ATTENDANCE FOR THE 25/DECEMBER YOU 
NEED TO PAY IN YOUR ENTRANCE FEE FROM <em>9/DECEMBER</em> IN STORE.</span><strong></p>
</div>
-->
<div><br /></div>
<h2 class="novogod">NEW YEARS EVE - 31 Dec 2013</h2>
<h2 class="third">КУВЕРТ- &pound;49 НА ЧОВЕК/ENTRANCE FEE &pound;49 PER PERSON</h2>
<div class="chrmenu2">
  <ul class="sitemap">
    <li><strong>САЛАТА/SALAD</strong>- РУСКА , СНЕЖАНКА , КАТЪК / RUSSIAN SALAD, MILK SALAD , KATAK </li>
    <li><strong>ТОПЛО ПРЕДЯСТИЕ/HOT STARTER</strong>- СЪРМИЧКИ СЪС СВИНСКО МЕСО И ОРИЗ / PORK MEAT WRAPED IN PICKLED CABBAGE LEAF AND RISE</li>
	<li><strong>ОСНОВНО ЯСТИЕ/MAIN COURSE</strong>- СВИНСКО ПЕЧЕНО РОЛЕ , ПИЛЕШКО ПЕЧЕНО РОЛЕ с ОРИЗ НА ФУРНА, КАРТОФЕНО ПЮРЕ, ТУРШИЯ/ ROASTED PORK ROLE, CHICKEN ROLE WITH RISE, MASHED POTATOES AND TURSIA.</li>
    <li><strong>ПЪРЛЕНКА/PARLENITSA</strong>- СЪС СИРЕНЕ И КАШКАВАЛ / WITH FETA CHEESE AND YELLOW CHEESE</li>
	<li><strong>БАНИЦА С КЪСМЕТЧЕТА / FETA CHEESE PIE</strong></li>
    <li><strong>СУХО МЕЗЕ/CURED MEAT</strong>- ЛУКАНКА,СУДЖУК, ФИЛЕ ЕЛЕНА, ПАСТЪРМА / LUKANKA, SUDZHUK, FILLET 'ELENA' , PASTURMA</li>
	<li><strong>ТИКВЕНИК / SWEET PUMPKIN PIE</strong></li>
    <li><strong>ЕДНО БЕЗАЛКОХОЛНО И ЕДНА БУТИЛКА МИНЕРАЛНА ВОДА / ONE FIZZY DRINK AND SMALL MINERAL WATER</strong></li>
	<li><strong>ШАМПАНСКО/CHAMPAGNE </strong>- ЕДНА БУТИЛКА НА ДВАМА ЧОВЕКА / ONE BOTTLE PER TWO(2) PEOPLE</li>
    <li><strong>ЧАША КАФЕ/A CUP OF COFFEE</strong></li>
</ul><br />
<p><strong>ВСИЧКИ ГОСТИ СА ДОБРЕ ДОШЛИ ОТ 19:00 ЧАСА И РЕСТОРАНТА 
ЩЕ Е ОТВОРЕН ДО РАННИТЕ ЧАСОВЕ НА 01 ЯНУАРИ. <!--ЗА РЕЗЕРВАЦИИ  МОЛЯ ОБЪРНЕТЕ СЕ КЪМ ПEРСОНАЛА В РЕСТОРАНТА ИЛИ <a href="bookings.php">РЕЗЕРВИРАЙТЕ ОНЛАЙН</a>.-->
<span class="red">Съжаляваме,свободни места няма и резервациите са затворени!</span>.</strong></p>
<p>CUSTOMERS ARE WELCOME FROM 7PM AND THE RESTAURANT WILL BE OPEN TILL EARLY MORNING NEXT DAY. <!--FOR RESERVATIONS PLEASE CALL OR <a href="bookings.php">BOOK ONLINE</a> NOW.-->
 <span class="red">Reservations are now closed as we are fully booked!</span></p>

</div>
<!--
<script>

if(document.width< 480)
{
	$(".chrmenu,.chrmenu2").hide();
		$(".koledno").click(function(aa){
			$(".chrmenu").animate({'height' : 'toggle'}, 500, 'slow');
			
		 });
		$(".novogod").click(function(bb){
			$(".chrmenu2").animate({'height' : 'toggle'}, 500, 'slow');
			
		});

//}
</script>
-->
<!-- FOOTER ========================================================-->
<?php  													
$file3='../includes/footer.php';	
if(file_exists($file3) && is_readable($file3)){		
	include($file3);	
}
?>
