<?php 													// include the header.php(styles,scripts,meta)
	$file='../includes/header.php'; 
	if(file_exists($file) && is_readable($file)){
		include($file);
	}
?>
		
		<meta name="description" content="404-File Not Found on Bolyary Restaurant website" />
	</head>

	<body itemscope itemtype="http://schema.org/WebPage">
	<!-- GoogleTagManager =======================================================-->

<?php 													
	
$file='../includes/tagmanager.php'; 	
if(file_exists($file) && is_readable($file)){		
	include($file);
		}
?>
			<!-- HEADER  =======================================================-->

<header><span itemscope itemtype="http://schema.org/Restaurant">
  <h1 itemprop="name">Bolyary Restaurant</h1>
  <div class="hiding"></div>
  <p class="reservations"><!-- Book a table:<a href="bookings.php">Bookings form</a> <br />-->
    Call us: <span itemprop="telephone"><?php echo "{$tel}"; ?></span> </p>
</span></header>

			<!-- END HEADER  ===================================================-->			
			<!-- MENU  =========================================================-->
			
			<nav>
				<?php  									//include the menu.php
					$file2='../includes/menu.php';
					if(file_exists($file2) && is_readable($file2)){
						include($file2);
					}
				?>
				
			<!--	<ul id="breadcrumbs" itemprop="breadcrumb">                         
  					<li class="first"><a href="index.php">Home</a></li>
  					<li> &gt;&gt; <em><?php echo "{$title}"; ?></em></li>
  				</ul> -->

			</nav>
			<hr />
			<!-- END MENU  =====================================================-->
			<h1>404-error</h1>
			<h2>The page you are looking for doesn't exist!</h2>
			
			<div class="left">
				<p>Please, use the navigation menu to browse our site!</p>
				<p>We apologize for any inconvenience caused.</p>
			</div>
			<div class="right">
				
								
				
			</div>
			<!-- form starts  =====================================================-->
			
			
			<section>
							
			</section>
			<!-- END form  =====================================================-->
					
			<!-- FOOTER ========================================================-->
<?php  													//include the footer.php
	$file3='../includes/footer.php';
	if(file_exists($file3) && is_readable($file3)){
		include($file3);
	}
?>
			
			