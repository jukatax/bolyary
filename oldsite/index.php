<?php 													// include the header.php(styles,scripts,meta)
	$file='../includes/header.php';
	if(file_exists($file) && is_readable($file)){
		include($file);
	}
?>
<meta name="description" content="Bulgarian Restaurant in London-Best East European/Mediterranean Restaurant in North London" />
<meta name="keywords" content="Bulgarian Restaurant,Local Restaurant,Bulgarian cousin,Bulgarian Restaurant in North London,East European Cousin,Palmers Green Restaurant" />
<link rel="stylesheet" href="css/flexslider.css" type="text/css" />
<link rel="stylesheet" href="css/li-scroller.css" type="text/css" />
<!--<script  type="text/javascript" src="js/lightbox.js"></script>-->
<script  type="text/javascript" src="js/jquery.flexslider-min.js"></script>
<script  type="text/javascript" src="js/jquery.li-scroller.1.0.js"></script>
<style>
.flexslider {
	width: 74%;
	float: left;
}
.side_container {
	width: 24%;
	float: left;
	margin-left: 1%;
}
ul.side_images {
	list-style: none;
}
ul.side_images img {
	width: 98%;
	margin: 2px;
}
ul.side_images li {
	width: 98%;
	margin: 2px;
	display: inline;
}
ul.side_images:first-child {
	margin-top: 0;
}
div#front_text{
	float: left;
	clear:both;
	padding:1em;
	margin:1em auto;
	border:solid 1px #fff;
	border-radius:5px;
	background:#490D00;
	background:rgba(0,0,0,0.55);
	}
#front_text p{margin:.5em auto;letter-spacing:1px;color:#EAB280;font: 1.1em/120% Verdana,Helvetica;}
.text_slide{font-size:1.25em;}
.text_slide p{padding-right:2em;padding-top:1.25em;}
.text_slide a{text-decoration:none;width:100%;height:100%;}
.text_slide h2{color:red;}
.slide_link{color:#111;text-decoration:underline;}

/*search google styles*/

input{color:#000;}

 @media only screen and (min-width:481px) and (max-width:768px) {
.flexslider {
	width: 98%;
	max-width: 640px;
	float: none;
	margin: 0 auto;
}
.side_container {
	width: 99%;
	float: left;
	margin-left: 1%;
	margin-top: 40px;
}
ul.side_images img {
	width: 29%;
	margin: 1%;
}
ul.side_images li {
	width: 29%;
	margin: 1%;
	display: inline;
}
}
 @media only screen and (max-width:480px) {
.flexslider {
	width: 98%;
	max-width: 640px;
}
.side_container {
	width: 99%;
	float: left;
	margin-left: 0.25%;
}
ul.side_images img {
	width: 45%;
	margin: 1%;
}
ul.side_images li {
	width: 45%;
	margin: 1%;
	display: inline;
}
ul.side_images li:last-child {
	margin-left: 27%;
}
.text_slide{font-size:1em}
#front_text p{font: .9em/110% Verdana,Helvetica;}
}

.ticker_top,#ticker01{width:100%;}


</style>
</head>
<body itemscope itemtype="http://schema.org/WebPage">
<!-- GoogleTagManager =======================================================-->

<?php 													
/*	
$file='../includes/tagmanager.php'; 	
if(file_exists($file) && is_readable($file)){		
	include($file);
		}
		*/
?>

<!--<script type='text/javascript' src='js/snow.js'></script>-->
<!--==============================================================================================-->
<!--============================== HEADER  =======================================================-->
<!--==============================================================================================-->

<header itemscope itemtype="http://schema.org/Restaurant">
  <h1 itemprop="name">Bolyary Restaurant</h1>
  <div class="hiding"></div>
  <p class="reservations"><!-- Book a table:<a href="bookings.php">Bookings form</a> <br />-->
    Call us: <span itemprop="telephone" onClick="ga('send', 'event', 'Click', 'Call', 'ClickCall');"><?php echo "{$tel}"; ?></span> </p>
</header>

<!--==============================================================================================-->
<!--============================== END HEADER  ===================================================-->
<!--==============================================================================================-->

<!--==============================================================================================-->
<!--============================== MENU  =========================================================-->
<!--==============================================================================================-->
<nav>
  <?php  									//include the menu.php
	$file2 = '../includes/menu.php';
	if (file_exists($file2) && is_readable($file2)) {
		include ($file2);
	}
		?>
</nav>

<!--==============================================================================================-->
<!--============================== site search  =========================================================-->
<!--==============================================================================================-->
<!--
<script type="text/javascript">
// Google Internal Site Search script- By JavaScriptKit.com (http://www.javascriptkit.com)
// For this and over 400+ free scripts, visit JavaScript Kit- http://www.javascriptkit.com/
// This notice must stay intact for use
//Enter domain of site to search.
var domainroot="bolyary.co.uk"

function Gsitesearch(curobj){
curobj.q.value="site:"+domainroot+" "+curobj.qfront.value
}
</script>


<form action="http://www.google.com/search" method="get" onSubmit="Gsitesearch(this)">

<p style="text-align:left;font:italic .8em  Helvetica,serif;">
	Search Bolyary Restaurant for:<br />
	<input name="q" type="hidden" />
	<input name="qfront" type="text" style="width: 180px" /> 
	<input type="submit" value="Search" />
</p>

</form>

<div class="searchg">
<script>
  (function() {
    var cx = '000197158529147137415:njsp-wmpzly';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>
</div>

-->
<hr />
<!--==============================================================================================-->
<!--============================== END MENU  =====================================================-->
<!--==============================================================================================-->
<h2 id='main' style="font-size: 1.35em;">Bulgarian Restaurant - Pizza, Grill, Satch, Gyuvetch</h2>
<!--<h2 style="font-size: 1.25em;">Pizza, Grill, Fish, Soups ,Coffees</h2>-->
<h3 class="ohours" style="font-size: 0.85em;">Wed-Fri: 17:00-23:00 , Sat-Sun: 11:00-0:00<br />Today we are 
<?php
$date=date("Y-m-d");
$wday=date('l', strtotime($date));
$array=array('Monday','Tuesday');
$closed=0;
foreach($array as $key=>$value){
		if(stristr($wday,$value) ){$closed=1;}
	}
if($date=='2014-04-03'){echo ' closed due to staff holiday';}
else if($date=='2014-04-02'){echo ' fully booked';}
else if($closed==1){echo ' closed.';}
else{echo ' open.';}
?>
</h3>
<!-- -->
<!--==============================================================================================-->
<!--======================================== TICKER ==============================================-->
<!--==============================================================================================-->
<div class='ticker_top'>
	<ul id="ticker01">
		<li><a href="foodmenu.php">Latest offers</a></li><!---->
		<li><a href="foodmenu.php">Free delivery 5pm-10pm(Wed-Sun) within 2 miles and orders over &#163;18.</a></li>
    	<li><span>&nbsp;</span><a href="foodmenu.php">New Family Trays to preorder</a></li>
		<li><span>&nbsp;</span><a href="http://www.facebook.com/Bolyary.Pizza.Grill.Restaurant">Like us on Facebook</a></li>
		<li><span>&nbsp;</span><a href="https://twitter.com/Bolyary">Follow @Bolyary on Twitter</a></li>
		<!--<li><span>14/07/2013</span><a href="foodmenu.php">Seafood Menu available</a></li>
		<li><span>01/06/2013</span><a href="foodmenu.php">Buy 1 get one half price on all pizzas.</a></li>
		<li><span>&nbsp;</span><a href="http://www.facebook.com/Bolyary.Pizza.Grill.Restaurant">Like us on Facebook</a></li>-->
	</ul>
</div>
<!--==============================================================================================-->
<!--==================================== SLIDER ==================================================-->
<!--==============================================================================================-->
<div class="flexslider">
  <ul class="slides">
  	<!--<li class="text_slide">
		<a href="christmas_menu.php">
			<h2>?????? ????????/Happy Holidays</h2>
			<h2>We are fully booked for New Year's Eve!</h2>
			
        </a>
    </li>
	<li> <a href="foodmenu.php"><img src="imgs/home/promo.jpg" alt="Mixed Grill for Four" /> </a></li>-->
	<li> <img src="imgs/home/zapekanka.jpg" alt="zapekanka" /> </li>
	<li> <a href="foodmenu.php"><img src="imgs/home/catering.jpg" alt="Catering info Bolyary restaurant view inside"/></a> </li>
	<li> <img src="imgs/home/home2.jpg" alt="Bolyary restaurant - bar view"/> </li>
  	<li> <a href="foodmenu.php"><img src="imgs/home/home8.jpg" alt="Family trays to preorder" /></a></li>
    <li> <img src="imgs/home/home1.jpg" alt="Grill Dishes"/> </li>
    <!---->
    <!---->
    <li> <img src="imgs/home/home3.jpg" alt="Satch,lamb shank,mushroom pancake"/> </li>
    <!--<li> <img src="imgs/home/discount_f.jpg" alt="Discount" /> </li>   
    <li> <img src="imgs/home/home4.jpg" alt="Gyuvech,pancake,grill"/> </li>-->
    <!--<li> <img src="imgs/home/discount2.jpg" alt="Discount" /> </li>
    <li> <img src="imgs/home/home5.jpg" alt="Bolyary Restaurant at night"/> </li>-->
    <!--<li> <img src="imgs/home/discount2.jpg" alt="Discount" /> </li>
    <li> <img src="imgs/home/home6.jpg" alt="Restaurant customers,cheers!" /> </li>-->
  </ul>
</div>

<div class="side_container">
  <ul class="side_images">
    <li> <a href="foodmenu.php"><img src="imgs/home/menu.jpg" alt="Delicious menu"/></a> </li>
    <li> <a href="bookings.php"><img src="imgs/home/bookings.jpg" alt="Online bookings"/></a> </li>
    <li> <a href="location.php"><img src="imgs/home/location.jpg" alt="Bolyary Restaurant on Google maps"/></a> </li>
  </ul>
</div>

<div id="front_text">
	<p>Bolyary Bulgarian Restaurant offers a variety of grills , stewed dishes , burgers , fish , pizzas and desserts for every taste.</p>
	<p>Traditional Bulgarian and East European dishes including: Kavarma , Satch , Gyuvetch and Shepherds Salad ,Mixed Grills and the all loved home-made  Biscuit Cake.</p>
	<p>There are a variety of Vegetarian dishes and a range of Red and White Wine and Bulgarian Beer.</p>
	<p>Full Bar with the most famous alcohol drinks included the traditional Bulgarian Rakiya.</p>
	<p>Give us a call, make a <a href="bookings.php">Reservation</a> or just pop-in for a delicious affordable and exotic meal.</p>
</div>
<!--==============================================================================================-->
<!--=======================// Flexslider JS call ====================================================-->
<!--==============================================================================================-->
<script type="text/javascript">
// Can also be used with $(document).ready()
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide"
  });
});
</script>
<!--==============================================================================================-->
<!--=======================// News ticker JS call ====================================================-->
<!--==============================================================================================-->
<script>

$(function(){
    $("ul#ticker01").liScroll({travelocity: 0.1});
});
</script>

<!--
	<div id="photos">
		<img src="imgs/home/home1.jpg" alt="Grill dishes"/>
		<img src="imgs/home/home2.jpg" alt="Kavarma,chips,beer,pizza"/>
		<img src="imgs/home/home3.jpg" alt="Sach,lamb shank,mushroom pancake"/>
		<img src="imgs/home/home4.jpg" alt="Gyuvech,pancake,grill"/>
		<img src="imgs/home/home5.jpg" alt="Bolyary restaurant front night view "/>
		<img src="imgs/home/home6.jpg" alt="Restaurant customers,cheers!" class='show' />
	</div>

	<script type="text/javascript">
		slideshow();
	</script>
-->
<script>
		var iev = navigator.userAgent.substr(navigator.userAgent.indexOf("MSIE")+4,3);
		if(navigator.userAgent.indexOf("MSIE")!=-1){
			if(iev<9){alert("You are using very old version of Internet Explorer - IE "+iev+" The website might not display correctly .Please update your browser or use Chrome or Firefox.");}
		}
</script>
<!--==============================================================================================-->
<!--============================== FOOTER ========================================================-->
<!--==============================================================================================-->

<?php  													//include the footer.php
	$file3 = '../includes/footer.php';
	if (file_exists($file3) && is_readable($file3)) {
		include ($file3);
	}
	?>
