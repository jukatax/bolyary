<?php 													// include the header.php(styles,scripts,meta)
	$file='../includes/header.php';
	if(file_exists($file) && is_readable($file)){
		include($file);
	}
?>

		<meta name="description" content="Table booking confirmation with Bolyary Bulgarian  Restaurant in London" />
		<meta name="keywords" content="Reservation confirmation,Booking confirmation" />
  <!--      <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
        <style>
			.ui-datepicker { width:auto;}
		</style>
  		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script>
			$(function() {
				$( "#datepicker" ).datepicker();
			  });
	  </script>
      -->
      <script type='text/javascript' src="webforms2.js"></script>
	</head>

	<body itemscope itemtype="http://schema.org/WebPage">
			<!-- HEADER  =======================================================-->

<header><span itemscope itemtype="http://schema.org/Restaurant">
  <p class="head1"><a href="index.php">
<span itemprop="name">Bolyary Restaurant</span></a></p>
  <div class="hiding"></div>
  <p class="reservations"><!-- Book a table:<a href="bookings.php">Bookings form</a> <br />-->
    Call us: <span itemprop="telephone" onClick="ga('send', 'event', 'Click', 'Call', 'ClickCall');"><?php echo "{$tel}"; ?></span> </p>
</span></header>

			<!-- END HEADER  ===================================================-->
			<!-- MENU  =========================================================-->

			<nav>
				<?php  									//include the menu.php
					$file2='../includes/menu.php';
					if(file_exists($file2) && is_readable($file2)){
						include($file2);
					}
				?>

				<!--<ul id="breadcrumbs" itemprop="breadcrumb">
  					<li class="first"><a href="index.php">Home</a></li>
  					<li> &gt;&gt; <em><?php echo "{$title}"; ?></em></li>
  				</ul> -->

			</nav>
			<hr />
			<!-- END MENU  =====================================================-->
			<h1 id='main'>Book a table</h1>
			<h2 class="ohours" style="font-size: 0.85em;">
			<?php
			$date=date("Y-m-d");
			$wday=date('l');
			$array=array('Monday','Tuesday');
			$closed=0;
			echo 'Today '.$wday.' we are ';
			foreach($array as $key=>$value){
					if(stristr($wday,$value) ){$closed=1;}
				}
			if($date=='2014-04-03'){echo ' closed due to staff holiday';}
			else if($date=='2014-04-02'){echo ' fully booked';}
			else if($closed==1){echo ' closed';}
			else{echo ' open';}
			?>
			</h2>
			<!-- <h2 class="discount">10% off your bill when you book for Monday-Thirsday</h2>-->
			<?php

			function printform(){
					echo "<section class='form'>
							<form method='post' action='submit_form.php'>
								<div class='formentry'>
								<label for='name'>Name&#42;:</label><br />
								<input type='text' name='name' id='name' value='".$_POST['name']."' placeholder='Enter your name' required='required' autofocus size='40' />
								</div>

								<div class='formentry'>
								<label for='email'>Email&#42;:</label><br />
								<input type='email' name='email' id='email' value='".$_POST['email']."' placeholder='Enter your email' required='required' size='40'  />
								</div>

								<div class='formentry'>
								<label for='tel'>Mobile&#42;:</label><br />
								<input type='tel' name='tel' id='tel' value='".$_POST['tel']."' placeholder='Your mobile eg.07123456789' size='40'  />
								</div>

								<div class='formentry'>
								<label for='number'>Number of people attending&#42;:</label><br />
								<input type='number' name='number' id='number' min='2' max='30' step='1' value='2' required='required' />
								</div>

								<div class='formentry'>
								<label for='date'>Preferred date&#42;:</label><br />
								<input type='date' name='date' id='date' value='".$_POST['date']."'  required='required'   />
								</div>

								<div class='formentry'>
								<label for='time'>Preferred time&#42;:</label><br />
								<input type='time' name='time' id='time' value='".$_POST['time']."' required='required'  />
								</div>
								<!--
								<div class='formentry'>
								<label for='sentfrom'>Device you are sending from:</label><br />
								<select name='sentfrom' id='sentfrom'>
								  <option value='PC' selected='selected'>PC</option>
								  <option value='HTC'>HTC</option>
								  <option value='Samsung'>Samsung</option>
								  <option value='iPhone'>iPhone</option>
								  <option value='Blackberry'>Blackberry</option>
								  <option value='Nokia'>Nokia</option>
								  <option value='iPad'>iPad</option>
								  <option value='HTC_Tablet'>HTC Tablet</option>
								  <option value='Google_Tablet'>Google Tablet</option>
								  <option value='Samsung_Tablet'>Samsung Tablet</option>
								  <option value='Blackberry_Tablet'>Blackberry Tablet</option>

								</select>
								</div>
								-->


								<div class='formentry' >
								<label for='comments'>Any other comments:</label><br />
								<textarea  placeholder='Enter your comments here' name='comments' id='comments' cols='30' rows='5'>
								</textarea>
								</div>
								<!--
								<div class='formentry'>
								<label for='subscription'>I agree to receive emails from Bolyary Restaurant with special personalized discount vouchers!</label><br />
								<input type='checkbox' name='subscription' id='subscription' value='subscribed'    />
								</div>
								-->
								<div class='clearfix'></div>
								<p>By clicking 'Submit Reservation', I agree to the <a href='privacy.php'>Privacy Policy Statement</a></p>
								<input type='submit' value='Submit Reservation' id='submit' />
							</form>
						</section>
						<div class='formentry'>
							<p>Or just get in touch with us by <a href='mailto:bolyary.restaurant@gmail.com'>email</a> !</p>
						</div>
						
					";
					}

			function spamcheck($field)
				  {
				  //filter_var() sanitizes the e-mail
				  //address using FILTER_SANITIZE_EMAIL
				  $field=filter_var($field, FILTER_SANITIZE_EMAIL);

				  //filter_var() validates the e-mail
				  //address using FILTER_VALIDATE_EMAIL
				  if(filter_var($field, FILTER_VALIDATE_EMAIL))
				    {
				    return TRUE;
				    }
				  else
				    {
				    return FALSE;
				    }
				  }



				if (isset($_POST['email']))
					  {//if "email" is filled out, proceed

					  //check if the email address is invalid
					  $mailcheck = spamcheck($_REQUEST['email']);
					  $subtime = substr($_POST["time"],0,2);
					  $subdate = substr($_POST["date"],8,2);
					  $weekday=date('l', strtotime($_POST["date"]));
						  if ($mailcheck==FALSE)
						    {
						    echo "<span class='annotation'>Invalid email address!</span>";
						    printform();
						    }
						  elseif($_POST["email"]=="issa.wolfe@googlemail.com" || $_POST["tel"]=="07884268497"){
                            echo "<span class='annotation'>Sorry,You have been previously reported and therefore blocked for spam by the Webmaster !</span>";
                            printform();
                          }
						  elseif(trim($_POST["name"])==""){
						  	echo "<span class='annotation'>You need to provide your name for the reservation!</span>";
						  	printform();
						  }
						  elseif(trim($_POST["tel"])=="" || $_POST["tel"]==" " || preg_match('/^00359/', $_POST["tel"]) || preg_match('/^\+359/', $_POST["tel"]) || !preg_match('/^07[0-9]{9}$/', $_POST["tel"]) ){
						  	echo "<span class='annotation'>Invalid Mobile number! Only valid UK MOBILE NUMBERS are allowed!</span>";
						  	printform();
						  }
						  elseif(trim($_POST["number"])=="" || $_POST["number"]=="0"){
						  	echo "<span class='annotation'>Invalid number of people attending. You need to provide how many people are attending!</span>";
						  	printform();
						  }
						  elseif($_POST["date"]=="" || trim($_POST["date"])==""){
						  	echo "<span class='annotation'>Invalid date! You need to provide a date for the reservation!</span>";
						  	printform();
						  }
						  elseif( date('y')>substr($_POST["date"],2,2)){echo "<span class='annotation'>Date invalid! You need to provide a valid date for the reservation!</span>";printform();}
						  elseif( date('m')>substr($_POST["date"],5,2) && date('y')>=substr($_POST["date"],2,2) ){echo "<span class='annotation'>Date invalid! You need to provide a valid date for the reservation!</span>";printform();}
						  elseif( date('d')>substr($_POST["date"],8,2) && date('m')>=substr($_POST["date"],5,2) && date('y')>=substr($_POST["date"],2,2) ){echo "<span class='annotation'>Date invalid! You need to provide a valid date for the reservation!</span>";printform();}
						  
						  elseif(date('D', strtotime($_POST["date"]) )=="Mon" || date('D', strtotime($_POST["date"]))=="Tue"  ){
						  	echo "<span class='annotation'>Date invalid! It's ".$weekday." and we are closed!<br />Opening hours are: Wed-Fri 5:30pm - 11:00pm <br /> Weekends 11:00 - midnight.</span>";
						  	printform();
						  }
						  elseif((date('D', strtotime($_POST["date"]) )=="Wed" || date('D', strtotime($_POST["date"]))=="Thu" || date('D', strtotime($_POST["date"]))=="Fri" )&&($subtime<17)  ){
						  	echo "<span class='annotation'>Time invalid! It's ".$weekday." and we are closed before 17:30 !<br />Opening hours are: Wed-Fri 5:30pm - 11:00pm <br /> Weekends 11:00 - midnight.</span>";
						  	printform();
						  }
						  elseif($_POST["time"]=="" || trim($_POST["time"])==""){
						  	echo "<span class='annotation'>Time invalid! You need to provide a time for the reservation!</span>";
						  	printform();
						  }
						  else if($_POST["date"]=="2014-02-14" || $_POST["date"]=="2014-03-29" ){echo "<span class='annotation'>Limited or no tables left for ".$weekday." ".$_POST["date"]." !<br /> Please call <a href='tel:".$tel."' onClick=\"ga('send', 'event', 'Click', 'Call', 'ClickCall');\">".$tel."</a> to confirm</span>";
							printform();
						  } /**/
						  else if($_POST["date"]=="2014-04-02" || $_POST["date"]=="2014-04-03" ){echo "<span class='annotation'>We are sorry, we are closed today because of holiday,but open tomorrow Friday 04/04/2014. </span>";
							printform();
						  }
						  
						  else{//send email
						    $ip = getenv('HTTP_CLIENT_IP')?:getenv('HTTP_X_FORWARDED_FOR')?:getenv('HTTP_X_FORWARDED')?:getenv('HTTP_FORWARDED_FOR')?:getenv('HTTP_FORWARDED')?:getenv('REMOTE_ADDR')?:'Unknown';
						  	$cc="reni_5000@yahoo.com";
							$to="bolyary.restaurant@gmail.com";
							$subject="New reservation for ".$_POST["date"];
							$name=trim($_POST["name"]);
							//$surname=$_POST["surname"];
							$email=$_POST["email"];
							$tel=$_POST["tel"];
							$number=$_POST["number"];
							$date=$_POST["date"];
							$time=$_POST["time"];
							if(trim(htmlentities($_POST["comments"]))==''){$comments='No additional comments.';}else{$comments=trim(htmlentities($_POST["comments"]));}
							//$sentfrom2=$_POST["sentfrom"];
							$ourmail="bolyary.restaurant@gmail.com";
							$message="<h2>Hello Bolyary,</h2>"."\n ".
"<h3>This is a new booking request</h3>" ."\n ".
"<p>From: " .$name."</p>\n ".
"<p>Email: ".$email. "</p>\n".
"<p>Mobile number: ".$tel. "</p>\n".
"<p>For the date: " .$date. "</p>\n".
"<p>At: " .$time. " o'clock" ."</p>\n".
"<p>For: " .$number. " people" ."</p>\n".
"<p>Comments: ".$comments."</p>\n".
"<p>Sent from: " .$sentfrom2." IP=".$ip."</p>\n".
"<p>Make sure You <span style='color:red;'>PRINT</span> this email!!!</p>";
//Sednd a confirmation email to the customer ======================================================================
$message2="<h2>Dear ".$name.",</h2>\n<p>This email confirms your reservation with Bolyary Restaurant.</p>
<p>Here is a copy of your booking:</p>
<p>Your name(s): " .$name."</p>" ."\n "."<p>Your contact Email: ".$email. "</p>" ."\n "."<p>Your Mobile number: ".$tel. "</p>" ."\n "."<p>You've just made a reservation for the date: " .$date. "</p>" ."\n "."<p>At: " .$time. " o'clock". "</p>\n<p>Comments: ".$comments."<br /><p>This is an automated email!The email address is not monitored ,so please <em><strong>do not reply</strong></em>!</p><p>If the information above is not correct ,please contact us on <strong>07838023691</strong> or at <strong>".$ourmail."</strong></p>";
							// Always set content-type when sending HTML email
							$headers = "MIME-Version: 1.0" . "\r\n";
							$headers .= "Content-type:text/html;charset=utf-8" . "\r\n";

							// More headers
							$headers .= "From: " .$email. "\r\n";;
							$headers .= "Cc: " . $cc. "\r\n";
							//send email to us
							mail($to,$subject,$message,$headers);
							//send email back to the customer with his submitted data
							mail($email,'Bolyary-Reservation-Do Not Reply' ,$message2,"MIME-Version: 1.0" . "\r\n"."Content-type:text/html;charset=utf-8" . "\r\n"." From: ".$ourmail. "\r\n");
							//print success message on the page
							echo"<section class='submition_results'>
							           <h2>Booking Successful</h2>
										<p>Thank You, <span class='annotation2'>".$name."</span> for booking with us!Here is a summary of your reservation:</p>
											<ul>
												<li>Reservation Date:<span class='annotation2'>".$date."</span></li>
												<li>Time:<span class='annotation2'>".$time."</span> o'clock</li>
												<li>People attending:<span class='annotation2'>".$number."</span></li>
												<li>Comments:<span class='annotation2'>".$comments."</span></li>
											</ul>
										<p>A confirmation email has been sent to <strong>".$email."</strong>!</p>
										
									</section>";
							printform();

							//------------------------Store details for subscribed customers---------------------
							$counter_file=fopen('../includes/counter.txt','r');
							$customers_file=fopen('../includes/customers.txt','r');

								$i=file_get_contents("../includes/counter.txt"); //read the current number for counter stored in the file
								$name=$_POST['name'];
								//$surname=$_POST['surname'];
								$email=$_POST['email'];
								$today=date("l dS \of F Y h:i:s A"); //format- Tuesday 27th of December 2013 05:23:45 pm
								$checkbox=$_POST['subscription'];

							if(isset($checkbox) && isset($name) && isset($email))
								{
									$file = file_get_contents('../includes/customers.txt');    //Reads a file into a string -open customers file to find if customer already exists
									if(stristr($file, $email) == FALSE) //Finds the position of the first occurrence of a string inside another string (case-insensitive)
									{	//If customer not in the list
									   	file_put_contents("../includes/customers.txt","{$i} {$name} - {$email} - {$today}\r\n",FILE_APPEND); //add new data + current count number
										$i++;             //increase counter by one
										file_put_contents("../includes/counter.txt","{$i}"); // store new counter value
									}
									else{ //if customer already in the list then close files
										fclose($counter_file);
										fclose($customers_file);
									}
								}
								else{//if checkbox/name/email is not set then exit procedure
									fclose($counter_file);
									fclose($customers_file);
								}



							// -----END Storing procedure -------------------------------------------------------


						  	}
					  	}
				else{//if "email" is NOT filled out, print form

				printform();
				}
			?>
			<!-- <section class='submition_results'>
				<p>Thank You, <span class='annotation'><?php  echo '$name'; ?></span> for booking with us!Here is a summary of what you have submitted to us:</p>
					<ul>
						<li>Reservation Date:<span class='annotation'><?php echo '$date'; ?></span></li>
						<li>Time:<span class='annotation'><?php echo '$time'; ?> </span>o'clock</li>
						<li>People attending:<span class='annotation'><?php echo '$number'; ?></span></li>
					</ul>
				<p>We will confirm with you shortly!</p>
				<p>See you soon and have a nice day!</p>
			</section>

// if (file_exists($_FILES['uploaded']['tmp_name'])) {
//    $mail->AddAttachment($_FILES['uploaded']['tmp_name'], $_FILES['uploaded']['name']);
//}
-->





<!-- FOOTER ========================================================-->
<?php  													//include the footer.php
	$file3='../includes/footer.php';
	if(file_exists($file3) && is_readable($file3)){
		include($file3);
	}
?>