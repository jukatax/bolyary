<?php 													// include the header.php(styles,scripts,meta)
	$file='../includes/header.php'; 
	if(file_exists($file) && is_readable($file)){
		include($file);
	}
?>
		 <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<meta name="description" content="Book a table for nice meal at Bolyary Restaurant in North London" />
		<meta name="keywords" content="Bookings,Restaurant booking,Restaurant reservation,Free booking" />
  <!--      <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
        <style>
			.ui-datepicker { width:auto;}
		</style>
  		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script>
			$(function() {
				$( "#datepicker" ).datepicker();
			  });
	  </script>
      -->
      <script type='text/javascript' src="webforms2.js"></script>
	</head>

	<body itemscope itemtype="http://schema.org/WebPage">
	<!-- GoogleTagManager =======================================================-->

<?php 													
	
$file='../includes/tagmanager.php'; 	
if(file_exists($file) && is_readable($file)){		
	include($file);
		}
?>
			<!-- HEADER  =======================================================-->

<header itemscope itemtype="http://schema.org/Restaurant">
  <p class="head1"><a href="index.php">
<span itemprop="name">Bolyary Restaurant</span></a></p>
  <div class="hiding"></div>
  <p class="reservations"><!-- Book a table:<a href="bookings.php">Bookings form</a> <br />-->
    Call us: <span itemprop="telephone" onClick="ga('send', 'event', 'Click', 'Call', 'ClickCall');"><?php echo "{$tel}"; ?></span> </p>
</header>

			<!-- END HEADER  ===================================================-->			
			<!-- MENU  =========================================================-->
			
			<nav>
				<?php  									//include the menu.php
					$file2='../includes/menu.php';
					if(file_exists($file2) && is_readable($file2)){
						include($file2);
					}
				?>
				
				<!--<ul id="breadcrumbs" itemprop="breadcrumb">                         
  					<li class="first"><a href="index.php">Home</a></li>
  					<li> &gt;&gt; <em><?php echo "{$title}"; ?></em></li>
  				</ul> -->

			</nav>
			<hr />
			<!-- END MENU  =====================================================-->
			<h1 id='main'>Book a table</h1>
			<h2 class="ohours" style="font-size: 0.85em;">
			<?php
			$date=date("Y-m-d");
			$wday=date('l');
			$array=array('Monday','Tuesday');
			$closed=0;
			echo 'Today '.$wday.' we are ';
			foreach($array as $key=>$value){
					if(stristr($wday,$value) ){$closed=1;}
				}
			if($date=='2014-04-25'){echo ' closed due to staff holiday';}
			else if($date=='2014-04-02'){echo ' fully booked';}
			else if($closed==1){echo ' closed.';}
			else{echo ' open';}
			?>
			</h2>
			<!--<h2 class="discount">10% off your bill when you book for Monday-Thirsday</h2> -->
			<!-- form starts  =====================================================-->
			<section class='form'>
				<form method='post' action='submit_form.php'>
					<div class='formentry'>
					<label for='name'>Name&#42;:</label><br />
					<input type='text' name='name' id='name' placeholder='Enter your name' required='required' autofocus size='40' />
					</div>
					
					<div class='formentry'>
					<label for='email'>Email&#42;:</label><br />
					<input type='email' name='email' id='email' placeholder='Enter your email' required='required' size='40'  />
					</div>
					
					<div class='formentry'>
					<label for='tel'>Mobile&#42;:</label><br />
					<input type='tel' name='tel' id='tel' placeholder='Your mobile eg.07123456789' size='40'  />
					</div>
					
					<div class='formentry'>
					<label for='number'>Number of people attending&#42;:</label><br />
					<input type='number' name='number' id='number' min='2' max='30' step='1' value='2' required='required' />
					</div>
					
					<div class='formentry'>
					<label for='date'>Preferred date&#42;:</label><br />
					<input type='date' name='date' id='date' required='required'   />
					</div>
					
					<div class='formentry'>
					<label for='time'>Preferred time&#42;:</label><br />
					<input type='time' name='time' id='time' required='required'    />
					</div>
					<!--
					<div class='formentry'>
					<label for='sentfrom'>Device you are sending from:</label><br />
								<select name='sentfrom' id='sentfrom'>
											  <option value='PC' selected='selected'>PC</option>
											  <option value='HTC'>HTC</option>
											  <option value='Samsung'>Samsung</option>
											  <option value='iPhone'>iPhone</option>
											  <option value='Blackberry'>Blackberry</option>
											  <option value='Nokia'>Nokia</option>
											  <option value='iPad'>iPad</option>
											  <option value='HTC_Tablet'>HTC Tablet</option>
											  <option value='Google_Tablet'>Google Tablet</option>
											  <option value='Samsung_Tablet'>Samsung Tablet</option>
											  <option value='Blackberry_Tablet'>Blackberry Tablet</option>
								  
								</select>
					</div>
					-->
					
					
					<div class='formentry'>
					<label for='comments'>Any other comments:</label><br />
					<textarea  placeholder='Enter your comments here' name='comments' id='comments' cols='30' rows='5'>
					</textarea>
					</div>
					<!--
					<div class='formentry'>
					<label for='subscription'>I agree to receive emails from Bolyary Restaurant with special personalized discount vouchers!</label>
					<input type='checkbox' name='subscription' id='subscription' value='subscribed' />
					</div>
					-->
					<div class='clearfix'></div>
					<p>By clicking 'Submit Reservation', I agree to the <a href="privacy.php">Privacy Policy Statement</a></p>
					<input id='submit' type='submit' value='Submit Reservation' />
				</form>
			</section>
			<div class='formentry'>
				<p>Or just get in touch with us by <a href="mailto:bolyary.restaurant@gmail.com">email</a> !</p>
			</div>
			<!--<div class='formentry'>
				<p><strong>Note:</strong></p>
				<p>Any no shows or no answer when we call you to confirm reservation will result in email/phone bann !</p>
			</div>
			
			
			-->
			<!-- END form  =====================================================
			<div class="book-right">
				<div class="addinfo">
					<h2>Additional info</h2>
					<p>Text here or image optional</p>
				</div>
				<div class="addinfo">
					<h2>Additional info</h2>
					<p>Text here or image optional</p>
				</div>
			</div>
				
			-->
            

			<!-- FOOTER ========================================================-->
<?php  													//include the footer.php
	$file3='../includes/footer.php';
	if(file_exists($file3) && is_readable($file3)){
		include($file3);
	}
?>
			
			