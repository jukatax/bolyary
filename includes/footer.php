<?php
	$company='Reneta &#38; Son Ltd.';
	ini_set('date.timezone', 'Europe/London');
    $startyear=12;
	$thisyear=date('y');  // displays last two digits of the year not 2012 but 12
?>
<!-- 
<div class="left_banner">
	<div class="open">&nbsp;</div>
	<div class="close">&nbsp;</div>
	<div class="offers">
		<img src="imgs/menu/seafood/fish_and_chips.jpg" width="200" height="182" alt="fish and chips" />
	</div>
    
	<h3>New Seafood menu in Bolyary restaurant</h3>
	<p><a href="foodmenu.php">Food menu</a> <br /><em>Price: &#163;10</em><br /></p>
	
</div>
-->
			<footer>	
				<p>	<span class="companyf">Copyright &copy;
					<?php 
					if($thisyear==$startyear){
						echo '20'.$startyear;
					}else{
						echo '20'."$startyear-"."$thisyear";
					}
					 
					?> 
					  <?php echo "$company";?></span>
<!--					  
					   | <a href="sitemap.php">Sitemap</a> |<a id="fb" href="http://www.facebook.com/sharer.php?u=http://bolyary.co.uk">
				<img src="social/f.png" width="18" alt="share on facebook icon" /></a> |
				<a id="tw" href="http://twitter.com/share?text=An%20Awesome%20Bulgarian%20Restaurant%20in%20North%20London&amp;url=http://bolyary.co.uk">
				<img src="social/t.png" width="18" alt="share on tweeter icon" /></a> |
-->				</p>
				<!-- g+1 button -->
  				<div id="gplus1"><div class="g-plusone" data-size="medium"></div>
					<script type="text/javascript">
					  (function() {
					    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
					    po.src = 'https://apis.google.com/js/plusone.js';
					    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
					  })();
					</script>
				</div>
                
				<!-- g+1 button end -->
				<p class="small"><span class="companyf">Design and development: </span><a href="http://yuliyan-yordanov.co.uk" rel="author">YDY</a></p>
				<!-- SortSite Small Badge v3 Start -->             
				<p class='bottom_footer'>
					<a href="privacy.php">Privacy Policy</a>
					<a href='http://www.powermapper.com/products/sortsite/' rel="nofollow">
						<img src='http://www.powermapper.com/images/badge-v1/sortsite-badge-small-10.png' width="80" height="15" alt='SortSite - top 10%' style='border: 0px'/>
					</a>

				</p>
<!-- SortSite Small Badge End -->                
                
			</footer>
			<!-- ==================scripts================================================================== -->
			
		
			<script type='text/javascript' src='js/respond.min.js'></script>
			<script type='text/javascript' src="js/vendor/modernizr-2.6.2.min.js"></script>

            <script>
			var initDatepicker = function() {
								$('input[type=date]').each(function() {
									var $input = $(this);
									$input.datepicker({
										minDate: $input.attr('min'),
										maxDate: $input.attr('max'),
										dateFormat: 'yy-mm-dd'
									});
								});
							};
							
							if(!Modernizr.inputtypes.date){
								$(document).ready(initDatepicker);
							};
			</script>
				
			
		
		
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-38550115-1', 'bolyary.co.uk');
  ga('require', 'linkid', 'linkid.js');
  ga('send', 'pageview');

</script>
			
			<script type="text/javascript">  //<!-- =========== Hide address bar after loading the page ===========-->
				window.addEventListener("load", function () { 
				// Set a timeout... 
				setTimeout(function () { 
				// Hide the address bar! 
				window.scrollTo(0, 1); 
				}, 0); 
				}); 
			</script>
			
			<script type="text/javascript"> //  <!-- smooth scroll  -->
				$(document).ready(function(){ 
					$('a[href^="#"]').bind('click.smoothscroll',function (e) {
					    e.preventDefault();
					 
					    var target = this.hash,
					    $target = $(target);
					 
					    $('html, body').stop().animate({
					        'scrollTop': $target.offset().top
					    }, 900, 'swing', function () {
					        window.location.hash = target;
					    });
					});
					
			
				}); 
		</script>
		
<script>
		var iev = navigator.userAgent.substr(navigator.userAgent.indexOf("MSIE")+4,3);
		if(navigator.userAgent.indexOf("MSIE")!=-1){
			document.createElement("article");
			document.createElement("footer");
			document.createElement("header");
			document.createElement("hgroup");
			document.createElement("nav");
			document.createElement("aside");
		}
</script>

	<!--	<script> // =========== left sliding banner ===========
		$(document).ready(function(){
			var height_banner=$('.left_banner').outerHeight();
			var height=($(window).height()-height_banner)/2; 
			//position the banner with script
			if(height<0){
				height=($(document).height()-height_banner)/2;
				if(height>300)
					{height=50;}
				}
			//position the window absolute if js available ,otherwise at bottom of the page
			$('.left_banner').css({'display':'block','position':'fixed','top':height+'px','right':'-190px'});
			//show arrow if js is available ,otherwise it will be hidden
			$('.open').css({'display':'block'});

			$('.open').click(function(){
				$('.left_banner').animate({right:'0'});
				$('.open').css({'display':'none'});
				$('.close').css({'display':'block'});
			});

			$('.close').click(function(){
				$('.left_banner').animate({right:'-190px'});
				$('.open').css({'display':'block'});
				$('.close').css({'display':'none'});
			});
			$(window).resize(function(){
				setTimeout(function(){
					var height=($(window).height()-height_banner)/2;
					$('.left_banner').css({'top':height+'px'});
				},500);
			});
		});	
		</script>
		-->

	</body>
</html>