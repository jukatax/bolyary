<?php
    $title=basename($_SERVER['SCRIPT_NAME'],'.php');
	if($title=='index'){
		$title='Bolyary Bulgarian Restaurant in London';
	}
	else if($title=='foodmenu'){
		$title='Traditional Bulgarian dishes and grill';
	}
	else if($title=='bookings'){
		$title='Reservations/Bookings at Bolyary Bulgarian Restaurant';
	}
	else if($title=='location'){
		$title='Find Bolyary in Palmers Green,North London';
	}
	else if($title=='privacy'){
		$title='Bolyary Restaurant Privacy Statement';
	}
	else if($title=='sitemap'){
		$title='Bolyary Restaurant websites Sitemap';
	}
	else{
		$title=ucfirst($title);
		$title=str_replace('_',' ', $title);
	}
	//$tel='02035606819';
	$tel='07838023691';
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">

<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<title><?php echo "{$title}"; ?></title>
<meta name="author" content="Yuliyan Yordanov" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="canonical" href="http://bolyary.co.uk" />
<!--  Mobile Viewport Fix
		j.mp/mobileviewport & davidbcalhoun.com/2010/viewport-metatag 
		device-width : Occupy full width of the screen in its current orientation
		initial-scale = 1.0 retains dimensions instead of zooming out if page height > device height
		maximum-scale = 1.0 retains dimensions instead of zooming in if page width < device width
		-->
<meta name="google-site-verification" content="">
<!-- STYLE SHEETS -->
<link rel="stylesheet" href="css/normalize.css" type="text/css" />
<link rel="stylesheet" href="css/styles.css" type="text/css" media="screen" />
<!-- FAVICONS -->
<link rel="shortcut icon" href="imgs/favicon.png" />
<link rel="icon" type="image/png" href="imgs/favicon.png" />
<link rel="apple-touch-icon-precomposed" sizes="114×114" href="imgs/apple-touch-icon-114×114-precomposed.png" />
<link rel="apple-touch-icon-precomposed" sizes="72×72" href="imgs/apple-touch-icon-72×72-precomposed.png" />
<link rel="apple-touch-icon-precomposed" href="imgs/touch-icon-iphone-precomposed.png" />
<!-- ==================scripts================================================================== -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
					
	

<!-- ================== end header scripts================================================================== -->
<?php
			$iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
			$iphone2 = stristr($_SERVER['HTTP_USER_AGENT'],"iPhone");
			$android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
			$android2 = stristr($_SERVER['HTTP_USER_AGENT'],"Android");
			$palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
			$palmpre2 = stristr($_SERVER['HTTP_USER_AGENT'],"webOS");
			$berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
			$berry2 = stristr($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
			$ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
			$ipod2 = stristr($_SERVER['HTTP_USER_AGENT'],"iPod");
			$nokia = stristr($_SERVER['HTTP_USER_AGENT'],"lumia");
			$nokia2 = strpos($_SERVER['HTTP_USER_AGENT'],"Lumia");
			$nokia3 = stristr($_SERVER['HTTP_USER_AGENT'],"nokia");
			$nokia4 = strpos($_SERVER['HTTP_USER_AGENT'],"NOKIA");
			$sentfrom2='unknown';
			if ($iphone == true || $iphone2 == true)
				{
					$sentfrom2='iPhone';
				}
			elseif ($android== true || $android2== true)
				{
					$sentfrom2="Android";
							}
			elseif ($berry == true || $berry2 == true)
				{
					$sentfrom2="Blackberry";
								}
			elseif ($ipod == true || $ipod2 == true)
				{
					$sentfrom2="iPod";
								}
			elseif ($palmpre== true || $palmpre2== true)
				{
					$sentfrom2="webOS";
								}
			elseif ($nokia== true || $nokia2== true ||$nokia3== true || $nokia4== true)
				{
					$sentfrom2="Nokia";
								}
			else
				{
					$sentfrom2="PC";
								}
								/*
	$x=$_SERVER['HTTP_USER_AGENT'];
	if(stripos($x,"bot")=="" && stripos($x,"crawl")=="" && stripos($x,"spider")=="" && stripos($x,"privacypolicy")=="" && stripos($x,"facebook")=="" && stripos($x,"robo")=="" ){	
			// location of the text file that will log all the ip adresses
		$file = '../includes/logfile.txt';

		// ip address of the visitor
		$ipadress = $_SERVER['REMOTE_ADDR'];

		// date of the visit that will be formated this way: 29/May/2011 12:20:03
		date_default_timezone_set('Europe/London');
		$date = date('d/F/Y H:i:s');

		// name of the page that was visited
		$webpage = $_SERVER['SCRIPT_NAME'];

		// visitor's browser information
		$browser = $_SERVER['HTTP_USER_AGENT'];

		// Opening the text file and writing the visitor's data
		$fp = fopen($file, 'a');

		fwrite($fp, $ipadress.' - ['.$date.'] '.$webpage.' '.$browser."\r\n");

		fclose($fp);
	}
	*/
		?>
