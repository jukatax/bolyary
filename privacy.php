<?php 													
	$file='includes/header.php'; 	
	if(file_exists($file) && is_readable($file)){		
	include($file);	
	}
	?>
	<meta name="description" content="Privacy Statement of Bolyary Bulgarian Restaurant" />
	<meta name="keywords" content="Privacy Statement Bolyary Restaurant" />
</head>

<body itemscope itemtype="http://schema.org/WebPage">

<!-- HEADER  =======================================================-->

<header> <span itemscope itemtype="http://schema.org/Restaurant">
 	<p class="head1"> 
	  <a href="index.php"> <span itemprop="name">Bolyary Restaurant</span> </a> 
	</p>
	<div class="hiding"></div>
	<p class="reservations"> Call us: <span itemprop="telephone"><?php echo "{$tel}"; ?></span> </p>

</header>
<!-- END HEADER  ===================================================--> <!-- MENU  =========================================================-->
<!--
<nav>
	
  <?php  								
  $file2='includes/menu.php';					
  if(file_exists($file2) && is_readable($file2)){						
  	include($file2);					
  }				
  ?>

</nav>

  -->
<hr />
<!-- END MENU  =====================================================-->
	<h1 id='main'>Privacy Policy</h1>
	<h2>Your privacy is important to us!</h2>
	<h3>So, we have developed a Privacy Policy that covers how we collect ,use,disclose,transfer and store your information</h3>
	<h3>Please,have a moment to familiarize yourself with this information</h3>
	<h4>Collection and Use of Personal Information</h4>
	<p>Personal information is data that can be used to uniquely identify or contact a single person.			You may be asked to provide your personal information anytime you are in contact with Bolyary Restaurant!			Bolyary Restaurant will NOT disclose your information to third-parties.Your information is stored after your consent and is used to send you emails with special personalized promotions.			The only personal information we store is your name and email.</p>
	<h4>Cookies and Other Technologies</h4>
	<p>Bolyary Restaurant does NOT use cookies to track or store your browsing habits.			We use Google Analytics technology to track how visitors are accessing our site to improve our services.This may include IP,Browser type and other information which is not stored or disclosed anywhere to the public.</p>
	<h4>Protection of Personal Information</h4>
	<p>Bolyary Restaurant takes extra precautions - administrative, technical and physical- to ensure the safety of your personal information!</p>
	<h4>Privacy Questions</h4>
	<p>If you have any questions or concerns about our Privacy Policy or data protection ,please don't hesitate to <a href='mailto:bolyary.restaurant@gmail.com?Subject=About%20Privacy%20Policy'>contact us</a>.To opt out of being tracked by Google Analytics across all websites visit <a href="http://tools.google.com/dlpage/gaoptout">http://tools.google.com/dlpage/gaoptout</a>. </p>
<!-- FOOTER ========================================================-->
<?php  												
	$file3='includes/footer.php';	
	if(file_exists($file3) && is_readable($file3)){		
	include($file3);	
	}
	?>
