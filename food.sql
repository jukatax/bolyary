
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 04, 2013 at 07:37 AM
-- Server version: 5.1.66
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u465187560_food`
--

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE IF NOT EXISTS `food` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary id',
  `foodname` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'food name',
  `price` float unsigned NOT NULL DEFAULT '0' COMMENT 'food price',
  `date_updated` date NOT NULL COMMENT 'date price updated',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='Food and their prices' AUTO_INCREMENT=41 ;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`id`, `foodname`, `price`, `date_updated`) VALUES
(1, 'pork shank', 15.8, '2013-03-01'),
(2, 'pork ribs', 7.8, '2013-03-01'),
(3, 'pork collar steak', 7.5, '2013-03-01'),
(4, 'pastirsko shareno', 7.5, '2013-03-01'),
(5, 'mixed grill for one', 8.2, '2013-03-01'),
(6, 'mixed grill for 4', 29.9, '2013-03-02'),
(7, 'gevrek', 7.2, '2013-03-01'),
(8, 'chicken strips', 7.8, '2013-03-01'),
(9, 'bolyarski kaik', 39.99, '2013-03-01'),
(10, 'chicken shashlik for 2', 15.5, '2013-03-01'),
(11, 'stuffed chicken steak', 8.5, '2013-03-01'),
(12, 'pork satch', 9.5, '2013-03-01'),
(13, 'kavarma', 8.8, '2013-03-01'),
(14, 'chicken surprise', 8.6, '2013-03-01'),
(15, 'spinach crockets', 7.5, '2013-03-01'),
(16, 'potatoes baked in foil', 6.5, '2013-03-01'),
(17, 'mushroom pancakes', 6.5, '2013-03-01'),
(18, 'mushroom crockets', 7.5, '2013-03-01'),
(19, 'mixed vegetarian platter', 18.8, '2013-03-01'),
(20, 'lentil crockets', 7.5, '2013-03-01'),
(21, 'courgette rolls', 7.8, '2013-03-01'),
(22, 'cauliflower crockets', 7.5, '2013-03-01'),
(23, 'carrot crockets', 7.5, '2013-03-01'),
(24, 'margarita', 5.5, '2013-03-01'),
(25, 'vegetarian', 5.8, '2013-03-01'),
(26, 'fungi', 5.8, '2013-03-01'),
(27, 'roma', 6.3, '2013-03-01'),
(28, 'adriano', 6.3, '2013-03-01'),
(29, 'caprichosa', 6.3, '2013-03-01'),
(30, 'pesto', 6.3, '2013-03-01'),
(31, 'frouti di mare', 6.8, '2013-03-01'),
(32, 'quatro stagioni', 6.3, '2013-03-01'),
(33, 'calzone', 6.5, '2013-03-01'),
(34, 'piperoni', 6.5, '2013-03-01'),
(35, 'bolyary', 7.3, '2013-03-01'),
(36, 'pleskavica', 7.5, '2013-03-01'),
(37, 'stuffed mushrooms', 5.4, '2013-03-01'),
(38, 'grilled vegetables', 7.5, '2013-03-01'),
(39, 'shopski guvech', 6.5, '2013-03-01'),
(40, 'roasted leg of lamb', 16.9, '2013-03-01');


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
